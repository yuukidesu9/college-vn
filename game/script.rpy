﻿# Esse é o script inteiro do jogo.

# O comando "define" é usado para criar variáveis.
# Pode ser usado tanto dentro de labels (variáveis locais)
# quanto fora deles (variáveis globais).
# As variáveis devem sempre ser inicializadas antes de
# serem usadas globalmente.
# Sintaxe: define <variável> = <valor>
# O comando "default" é usado para criar variáveis
# estáticas, que tem um valor fixo ao carregar o script.
# Sintaxe: default <variável> = <valor>
# define e = Character("nome", color="#XXXXXX", image="nome")
# Para definir um personagem, usa-se o tipo Character.
# A cor do texto é opcional.


default name = ""
default me = Character("Eu", color="#FFFFFF")
default mae = Character("Mãe", color="#FF0080")
default yuu = Character("Yuuki", color="#FF8000")
default duk = Character("Duk", color="#FFFF00")
default ryu = Character("Ryu", color="#0080FF")
default kat = Character("Kath", color="#00FFFF")
default jir = Character("Jiro", color="#C0A040")
default yet = Character("Yeti")
default rik = Character("Rika", color="#FF8000")
default unk = Character("????", color="#FFFFFF")
default gender = ""
default pronoun = ""
default course = ""
default courseid = 0

# A VN começa aqui.
# O comando "label" define novas ramificações, como se fossem funções.
# O label "start" é como a função main() em C, ou __main__ em Python.
# Sintaxe: label <nome>

label start:
    # Para mostrar planos de fundo, usa-se o comando "scene".
    # O plano de fundo preto (black) é embutido no próprio Ren'Py.
    # O nome da cena deve condizer com o nome do arquivo.
    # Exemplo: se for "parque dia.png", o nome da cena fica "parque dia".
    # O arquivo de plano de fundo deve estar na pasta "images" do projeto.
    # scene bg <nome da cena> with <transição>
    
    # O comando "with" pode ser usado tanto depois de personagens quanto
    # de planos de fundo.
    # Tipos de transição suportados:
    # - fade
    # - dissolve
    # - pixellate
    # - movein<left/right/top/bottom>/moveout<left/right/top/bottom>
    # - easein<left/right/top/bottom>/easeout<left/right/top/bottom>
    # - zoomin/zoomout/zoominout
    # - vpunch/hpunch
    # - blinds
    # - wipe<left/right/up/down>
    # - squares
    # - slide<left/right/up/down>/slideaway<left/right/up/down>
    # - push<left/right/up/down>
    # - irisin/irisout
    
    scene black

    # Ren'Py tem uma integração básica com Python em comandos!
    # Basta usar o bloco "python" dentro da label.
    python:
        # A função renpy.input dentro do bloco de Python permite
        # usar entrada de texto básica do usuário.
        # O comprimento é opcional.
        # Sintaxe: <variável> = renpy.input(<prompt>, length=<comprimento>)
        
        name = renpy.input("Insira seu nome, sem espaços:", length=32)
        name = name.strip()
        if not name:
            name = "Jogador"
    
    # O bloco "menu" permite criar um menu com botões
    # para entrada do usuário.
    
    # Sintaxe:
    # menu:
    #     "Cabeçalho/Texto do menu"
    #     "Opção":
    #         <comandos>
    #     "Opção":
    #         <comandos>
    
    menu:
        "Qual seu gênero?"
        
        "Masculino (ele/dele)":
            $ gender = "o"
            $ pronoun = "e"
    
        "Feminino (ela/dela)":
            $ gender = "a"
            $ pronoun = "a"
            
    
    scene bg quarto with dissolve
    
    # O comando "show" mostra um sprite de um personagem.
    # O procedimento é o mesmo que o da cena. Nome do sprite
    # deve coincidir com nome do arquivo, e deve estar na
    # pasta "images" do projeto.
    # Sintaxe: show <nome do sprite> with <transição> as <alias>
    #          at <posição> behind <outro sprite>
    show mae with dissolve
    
    # Para adicionar diálogo, existem dois ou mais jeitos.
    # Caso for o narrador, só um par de aspas duplas contendo o texto.
    # Sintaxe: "Texto"
    # Caso for algum personagem, use o nome do personagem, e se tiver
    # sprites com expressões, use-as também.
    # Use um @ para denotar que a expresão mostrada é temporária.
    # Sintaxe: <personagem> <expressão> "Texto"
    # Sintaxe: <personagem> @ <expressão> "Texto"
    # Caso mesclar as duas sintaxes, a primeira expressão ficará como
    # padrão na próxima fala, caso não for denotada nenhuma na próxima.
    # Sintaxe: <personagem> <expressão 1> @ <expressão 2> "Texto"
    # A tag {fast} no texto mostra o conteúdo do texto antes da mesma
    # instantaneamente, mesmo no modo lento.
    # A tag {w=X} denota uma espera. Caso houver um parâmetro, o mesmo é
    # a quantidade de segundos a esperar.
    # A tag {p=X} funciona da mesma maneira, porém pula uma linha. O parâmetro
    # é a quantidade de segundos a esperar antes de pular a linha.
    # A tag {nw} denota que não é necessária entrada do usuário para
    # continuar o texto após a mesma.
    # A tag {b}{/b} aplica negrito no texto.
    # A tag {i}{/i} aplica itálico no texto.
    # A tag {color="#xxxxxx"}{/color} aplica cor ao texto.
    # A tag {u}{/u} aplica sublinhado no texto.
    # A tag {s}{/s} aplica riscado no texto.
    # A tag {image=<nome do arquivo>} aplica uma imagem entre o texto.
    # Deve-se usar a tag {alt}{/alt} para denotar texto alternativo
    # à imagem caso for usado TTS.
    # A tag {alpha=X}{/alpha} controla a opacidade do texto.
    # Caso for usado +/-/*, a opacidade é somada/subtraída/multiplicada
    # da padrão ao invés de trocada definitivamente. Recomendado.
    # A tag {a=<link>}{/a} aplica um link ao texto. Pode ser usado com URLs
    # ou até mesmo para pular para outra label quando clicado.
    # Exemplo: "Você pode {a=jump:label2}pular para label 2{/a} se quiser."
    #          "Ou então {a=https://www.google.com/"}abrir o Google{/a}."
    
    mae "Bom dia,{fast}{w=0.5} flor do dia!{fast}{w=1.0} O que deu do vestibular?{w=0.3} Já sabe do resultado?{w=0.3} Conta pra mãe!"
    me "Aaaah... bom dia, mãe. Eu vi ontem à noite o resultado, e passei!"
    mae "Que bom! Parabéns, filh[gender]!{fast}{p=1.0}Qual era o curso que você tinha se inscrito mesmo...?"
    menu:
        me "Poxa vida, qual era o curso mesmo? Ah, é! Era..."
        
        "Sistemas de Informação":
            $ course = "Sistemas de Informação"
            $ courseid = 0
        "Ciência da Computação":
            $ course = "Ciência da Computação"
            $ courseid = 1
        "Marketing":
            $ course = "Marketing"
            $ courseid = 2
        "Administração":
            $ course = "Administração"
            $ courseid = 3
        "Medicina":
            $ course = "Medicina"
            $ courseid = 4
        "Psicologia":
            $ course = "Psicologia"
            $ courseid = 5
        "Artes Visuais":
            $ course = "Artes Visuais"
            $ courseid = 6
        "Educação Física":
            $ course = "Educação Física"
            $ courseid = 7
    me "Era [course], mãe."
    mae "Que legal!{fast}{w=0.5} Só te desejo sorte, tá?\nAgora se arruma pra gente levar os documentos na faculdade.\nTem café lá na cozinha."
    hide mae with dissolve
    "E assim começou um dos meus últimos dias antes da faculdade.{p=0.5}Eu me arrumei, tomei meu café da manhã e fui com minha mãe até a faculdade para regularizar minha matrícula e ajeitar umas coisas."
    scene black with dissolve
    "Caramba, onde estão meus modos? Eu sou [name], e estou prestes a entrar pra faculdade.\nAinda morro de saudades do ensino médio, mas estou mais do que pront[gender] para esse novo capítulo na minha vida."
    
    # Use o comando "return" para finalizar o jogo.
    # Sintaxe: return
    
    return

label prologo:

label cap01:
